#!/bin/bash
BIN_DIR=$HOME/.bin
LOG_DIR=$HOME/.log
VENVS_DIR=$HOME/.venvs
VENV=$VENVS_DIR/power_market_info

echo "=============="
echo "STARTING SETUP"
echo "=============="

echo ""
echo "Checking for venv directory"
echo "---------------------------"

if ! [ -d "$VENVS_DIR" ];
then
    mkdir "$VENVS_DIR"
    echo "created $VENVS_DIR"
else
    echo "$VENVS_DIR already exists"
fi

echo ""
echo "Checking for Python virtual environment"
echo "---------------------------------------"
if ! [ -d "$VENV" ];
then
    python -m venv $VENV
    echo "created python venv $VENV"
else
    echo "$VENV already exists"
fi

echo ""
echo "Installing pip dependencies"
echo "---------------------------"
$VENV/bin/pip install -r requirements.txt

echo ""
echo "Checking for bin directory"
echo "--------------------------"
if ! [ -d "$BIN_DIR" ];
then
    mkdir "$BIN_DIR"
    echo "created $BIN_DIR"
else
    echo "$BIN_DIR already exists"
fi

echo ""
echo "Checking for log directory"
echo "--------------------------"
if ! [ -d "$LOG_DIR" ];
then
    mkdir "$LOG_DIR"
    echo "created $LOG_DIR"
else
    echo "$LOG_DIR already exists"
fi

echo ""
echo "Installing script"
echo "-----------------"
cp spot.py $BIN_DIR/spot
chmod +x $BIN_DIR/spot

echo ""
echo "=============="
echo "FINISHED SETUP"
echo "=============="
