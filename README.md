# Power Market Data
CLI app for fetching and showing power market data from [entso-e](https://transparency.entsoe.eu/)

## Installation
The script can be installed with the setup script.
```bash
git clone https://gitlab.com/wime/PowerMarketInfo.git
bash setup.bash
```
The script assumes:
* Custom executable scripts are stored in `~/.bin/`
* **There are no other sctipts with the name `spot`**
* Python virtual environments are stored at `~/.venvs/`.
* The Python virtual enviroenment `power_market_info` is used for this purpose.
* Logfiles are stored in `~/.log/`

## Usage
To see the spotprice of a given pricearea run:
```bash 
spot --options
```

The options are
Additional options are:
```bash
--start now
```
The timestamp for which to start fetching data. Options are `today`, `now`, `d-num`, `YYYYmmddHHMM`.
`today` starts at midnight today. `now` starts with the current hour. 
`d-num` where `num` is an integer 1, 2, 3...starts with midnight today minus `num` number of days. Yesterday at midnight would be `d-1`.
Alternatively, the option to give the starttime with a stirng `YYYYmmddHHMM` could alos be given. If the time is omitted, midnight is assumed.

```bash
--days 2
```
The number of days for which to fetch data, starting with `--start`.

```bash
--area no2
```
The price area for which to fetch data.

```bash
--table / --no-table
```
If the result should be shown in tabular format.

```bash
--graph / --no-graph
```
If the result should be shown in a graph.

## Roadmap
- [x] Spotprice
- [ ] Market volumes
- [ ] Wind power
- [ ] ...
