#!/home/wime/.venvs/power_market_info/bin/python
from dataclasses import dataclass
import inspect
import os
import logging
import pandas as pd
from plotly.subplots import make_subplots
import plotly.graph_objs as go
import typer
from entsoe import EntsoePandasClient
from pywime import plotting

AREA_CODES = {
    "no1": "NO_1",
    "no2": "NO_2",
    "no3": "NO_3",
    "no4": "NO_4",
    "no5": "NO_5",
}


app = typer.Typer()


@dataclass
class Data:
    """Entsoe data."""

    df: pd.DataFrame
    area: str
    type: str

    def plot(self):
        """Plot dataframe."""
        plotting.set_template()
        fig = make_subplots()
        fig.add_trace(go.Scatter(x=self.df.index, y=self.df[self.area], name=self.area))
        fig.update_layout(title=self.type.capitalize(), width=900, height=400)
        fig.update_yaxes(ticksuffix=" €/MWh")
        plotting.kitty_show(fig)

    def print_table(self, date_format: str = "%Y-%m-%d %H:%M"):
        """Print dataframe."""
        df = self.df.copy()
        df.index = df.index.strftime(date_format)
        print(df.to_markdown(tablefmt="pretty"))


def _get_timestamp(string: str, timezone: str):
    """Transform string to pd.Timestamp."""
    if string == "today":
        ts = pd.Timestamp.today(tz=timezone)
        return ts.replace(hour=0, minute=0, second=0, microsecond=0)
    elif string == "now":
        ts = pd.Timestamp.today(tz=timezone)
        return ts.replace(second=0, microsecond=0)
    elif string[:2] == "d-":
        day = pd.Timestamp.today(tz=timezone)
        day = day.replace(hour=0, minute=0, second=0, microsecond=0)
        return day - pd.Timedelta(days=int(string[2:]))
    return pd.Timestamp(string, tz=timezone)


@app.command()
def main(
    area: str = "no2",
    start: str = "now",
    days: int = 2,
    table: bool = True,
    graph: bool = False,
):
    """Show spotprice."""
    logging.basicConfig(
        level="INFO",
        format="%(asctime)s %(levelname)s %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
        filename=f"{os.environ['HOME']}/.log/spotprice.log",
    )

    logging.info(f"{inspect.currentframe().f_code.co_name} - starting.")
    try:
        client = EntsoePandasClient(api_key=os.environ["ENTSOE_TOKEN"])
        logging.info("Connected: True.")
    except ConnectionError:
        logging.error("Connected: False.")
        logging.info(f"{inspect.currentframe().f_code.co_name} - finished.")
        exit()
    try:
        start = _get_timestamp(string=start, timezone="Europe/Oslo")
        end = start.replace(hour=0, minute=0) + pd.Timedelta(days=days)
        df = client.query_day_ahead_prices(AREA_CODES[area], start=start, end=end)
        logging.info("Data fetched: True.")
    except Exception as e:
        logging.error("Data fetched: False.")
        logging.debug(f"{e}")
        logging.info(f"{inspect.currentframe().f_code.co_name} - finished.")
        exit()
    df = df.round(1)
    df.name = area
    data_table = Data(df.to_frame(), area, "spotprice")
    if table:
        data_table.print_table()
    if graph:
        data_table.plot()
    logging.info(f"{inspect.currentframe().f_code.co_name} - finished.")


if __name__ == "__main__":
    app()
